
#Importing methods and classes for testing urls
from django.test import SimpleTestCase
#Here you can explore SimpleTestCase class: https://docs.djangoproject.com/en/2.2/topics/testing/tools/#simpletestcase
from django.urls import reverse, resolve
from budget.views import project_list, project_detail, ProjectCreateView

#Creating TestUrls class that inherits SimpleTestCase to implement test case
#Djagno treats like test every class, method and file starts with keyword test
class TestUrls(SimpleTestCase):

    #Testing lists url endpoint
    def test_lists_url_resolved(self):
        url = reverse('list')
        self.assertEquals(resolve(url).func, project_list)

    #Testing add url endpoint
    def test_lists_add_url_resolved(self):
        url = reverse('add')
        self.assertEquals(resolve(url).func.view_class, ProjectCreateView)

    #Testing detail url endpoint with slug
    def test_lists_detail_url_resolved(self):
        url = reverse('detail',args=['some-slug'])
        self.assertEquals(resolve(url).func, project_detail)