#importing TestCase and Client classes for testing
# Here the link for documentation: 
from django.test import TestCase, Client
from django.urls import reverse
from budget.models import Category, Project, Expense
import json

# Testing views
class TestViews(TestCase):
    # This method will invoked firstly and it is set 
    # initial variables of object that will be used in another test cases
    def setUp(self):
        self.client = Client()
        self.list_url = reverse('list')
        self.detail_url = reverse('detail', args=['project1'])
        self.project1 = Project.objects.create(
            name = 'project1',
            budget = 100
        )

    # Testing project list endpoint with GET method of HTTP
    def test_project_list_GET(self):
        response = self.client.get(self.list_url)
 
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'budget/project-list.html')

    # Testing project detail endpoint with GET method of HTTP
    def test_project_detail_GET(self):
        response = self.client.get(self.detail_url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'budget/project-detail.html')

    # Testing createing new expense with POST method of HTTP
    def test_project_detail_POST_adds_new_expense(self):
        Category.objects.create(
            project = self.project1,
            name = 'development'
        )

        response = self.client.post(self.detail_url, {
            'title': 'expense1',
            'amount': 1000,
            'category': 'development'
        })

        self.assertEquals(response.status_code, 302)
        self.assertEquals(self.project1.expenses.first().title, 'expense1')

    # Testing creating expense with invalid posting data
    def test_project_detail_POST_no_data(self):
        response = self.client.post(self.detail_url)

        self.assertEquals(response.status_code, 302)
        self.assertEquals(self.project1.expenses.count(), 0)

    # Testing deleting project detail with DELETE method of HTTP
    def test_project_detail_DELETE_deletes_expenses(self):
        category1 = Category.objects.create(
            project = self.project1,
            name = 'development'
        )

        Expense.objects.create(
            project = self.project1,
            title =  'expense1',
            amount = 1000,
            category = category1
        )

        response = self.client.delete(self.detail_url, json.dumps({
            'id': 1
        }))

        self.assertEquals(response.status_code, 204)
        self.assertEquals(self.project1.expenses.count(), 0)
